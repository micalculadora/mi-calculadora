package com.example.micalculadora;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.etInput)
    EditText etInput;
    @BindView(R.id.contentMain)
    RelativeLayout contentMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        configEditText();
    }

    private void configEditText() {
        etInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager input = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                input.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });
    }


    @OnClick({R.id.btnSeven, R.id.btnFour, R.id.btnOne, R.id.btnpoint, R.id.btnOcho, R.id.btnCinco,
            R.id.btnDos, R.id.btnCero, R.id.btnNueve, R.id.btnSeis, R.id.btnTres})
    public void onClickNumbers(View view) {
        final String valStr = ((Button) view).getText().toString();
        switch (view.getId()) {
            case R.id.btnCero:
            case R.id.btnOne:
            case R.id.btnDos:
            case R.id.btnTres:
            case R.id.btnFour:
            case R.id.btnCinco:
            case R.id.btnSeis:
            case R.id.btnSeven:
            case R.id.btnOcho:
            case R.id.btnNueve:
                etInput.getText().append(valStr);
                break;
            case R.id.btnpoint:
                final String operacion = etInput.getText().toString();

                final String operador = Metodos.getOperator(operacion);

                final int count = operacion.length() - operacion.replace(".", "").length();

                if (!operacion.contains(Constantes.POINT)
                        || (count < 2 && (!operador.equals(Constantes.OPERATOR_NULL)))) {
                    etInput.getText().append(valStr);
                }

                break;
        }
    }

    @OnClick({R.id.btnClear, R.id.btnDivision, R.id.btnMultiplicacion, R.id.btnResta, R.id.btnSuma, R.id.btnResultado})
    public void onClickControls(View view) {
        switch (view.getId()) {
            case R.id.btnClear:
                etInput.setText("");
                break;
            case R.id.btnDivision:
            case R.id.btnMultiplicacion:
            case R.id.btnResta:
            case R.id.btnSuma:
                resolve(false);

                final String operador = ((Button)view).getText().toString();
                final String operacion = etInput.getText().toString();

                final String ultimoCaracter = operacion.isEmpty()? "" :
                        operacion.substring(operacion.length() - 1);

                if (operador.equals(Constantes.OPERATOR_SUB)){
                    if (operacion.isEmpty() ||
                            (!(ultimoCaracter.equals(Constantes.OPERATOR_SUB)) &&
                            !(ultimoCaracter.equals(Constantes.POINT)))){
                        etInput.getText().append(operador);
                    }else {
                        if (!operacion.isEmpty() &&
                                !(ultimoCaracter.equals(Constantes.OPERATOR_SUB)) &&
                                !(ultimoCaracter.equals(Constantes.POINT))){
                            etInput.getText().append(operador);
                        }
                    }
                }
                break;
            case R.id.btnResultado:
                resolve(true);
                break;
        }
    }

    private void resolve(boolean fromResult) {

    }
}
